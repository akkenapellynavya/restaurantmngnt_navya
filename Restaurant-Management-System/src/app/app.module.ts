import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Route,Router} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { from } from 'rxjs';
import { CourseComponent } from './course/course.component';
import { CleanerComponent } from './cleaner/cleaner.component';
import { WaiterComponent } from './waiter/waiter.component';
import { CookingdepartmentComponent } from './cookingdepartment/cookingdepartment.component';
import { ServingComponent } from './serving/serving.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { BillpaymentComponent } from './billpayment/billpayment.component';
import { TablebookingComponent } from './tablebooking/tablebooking.component';
import { ParkingComponent } from './parking/parking.component';
import { RestroomsComponent } from './restrooms/restrooms.component';
import { ManagerComponent } from './manager/manager.component';
import { MenuComponent } from './menu/menu.component';
import { HomedeliveryComponent } from './homedelivery/homedelivery.component';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { SpecialCabinComponent } from './special-cabin/special-cabin.component';
import { BuffetSystemComponent } from './buffet-system/buffet-system.component';
import { BanquetHallComponent } from './banquet-hall/banquet-hall.component';
import { CandleLightDinnerComponent } from './candle-light-dinner/candle-light-dinner.component';
import { RatingComponent } from './rating/rating.component';


@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    CleanerComponent,
    WaiterComponent,
    CookingdepartmentComponent,
    ServingComponent,
    FeedbackComponent,
    BillpaymentComponent,
    TablebookingComponent,
    ParkingComponent,
    RestroomsComponent,
    ManagerComponent,
    MenuComponent,
    HomedeliveryComponent,
    SpecialCabinComponent,
    BuffetSystemComponent,
    BanquetHallComponent,
    CandleLightDinnerComponent,
    RatingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path:'course',component:CourseComponent},
      {path:'cleaner',component:CleanerComponent},
      {path:'waiter',component:WaiterComponent},
      {path:'menu',component:MenuComponent},
    
      {path:'cookingdepartment',component:CookingdepartmentComponent},
      {path:'serving',component:ServingComponent},
      {path:'SpecialCabin',component:SpecialCabinComponent},
      {path:'feedback',component:FeedbackComponent},
      {path:'billpayment',component: BillpaymentComponent},
      {path:'tablebooking',component:TablebookingComponent},

      {path:'BuffetSystem',component: BuffetSystemComponent},
      {path:'BanquetHall',component:BanquetHallComponent },
      {path:'CandleLightDinner',component:CandleLightDinnerComponent },
      {path:'parking',component:ParkingComponent},
      {path:'restrooms',component:RestroomsComponent},
      {path:'homedelivery',component:HomedeliveryComponent},
      {path:'Rating',component:RatingComponent },
      {path:'manager',component:ManagerComponent},
      
    ])
],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
