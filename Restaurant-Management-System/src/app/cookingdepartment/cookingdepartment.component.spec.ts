import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookingdepartmentComponent } from './cookingdepartment.component';

describe('CookingdepartmentComponent', () => {
  let component: CookingdepartmentComponent;
  let fixture: ComponentFixture<CookingdepartmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookingdepartmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookingdepartmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
