import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandleLightDinnerComponent } from './candle-light-dinner.component';

describe('CandleLightDinnerComponent', () => {
  let component: CandleLightDinnerComponent;
  let fixture: ComponentFixture<CandleLightDinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandleLightDinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandleLightDinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
