import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../restaurant/restaurant';

@Component({
  selector: 'app-homedelivery',
  templateUrl: './homedelivery.component.html',
  styleUrls: ['./homedelivery.component.styl']
})
export class HomedeliveryComponent  {

  restaurant:Restaurant = {
    customer:"Coustomer Coming ",
    cleaner:"Clean Boy Cleans And Supply  Water To Customers",
    waiter:"Waiter Takes Order From The Customer",
    cookingDepartment:"Cooking Department",
    foodServer:"Supply Boy Serves Food To The Customers",
    SpecialCabin:"special cabins for family and friends",
    feedback:"Taking feedback from customers",
    billPayment:"Bill Paying",
    BuffetSystem:"Charge to a plate and having unlimited food",
    BanquetHall:"Meeting purpose,Events like Birthdat Functions,Marriage Paries",
    CandleLightDinner:"Spending Special Time for Special one",
    Restrooms:" Restrooms provided for customers",
    parkingPlaces:"Parking Places are also provided to the customers",
    manager:"Manager Manages All Above Things",
    homeDelivery:"Some Restaurants provides Home Delivery",
    tableBooking:"Table Booking By phone call or in any other way",
    Rating:"Rating",
    soup:"SWEET CORN SOUP",
    chicken:"CHICKEN HOT & SOUR",
    vegsweetcorn:"VEG SWEET CORN",
    veghotsour:"VEG HOT & SOUR",
    creamoftomato:"CREAM OF TOMATO",
    chickenmanchurian:"CHIKEN MANCHURIAN",
   chillichicken:"CHILLI CHICKEN",
   pepperchicken:"PEPPER CHIKEN", 
   apollofish:"APOLLO FISH",
   chillyfish:"CHILLY FISH",
   looseprawns:"LOOSE PRAWNS",
   fishtikka:"FISHTIKKA",
  muttoncurry:"MUTTON CURRY",
  muttonmasala:"MUTTON MASALA",
  kheemamasala:"KHEEMA MASALA"
  }
  
}
