import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialCabinComponent } from './special-cabin.component';

describe('SpecialCabinComponent', () => {
  let component: SpecialCabinComponent;
  let fixture: ComponentFixture<SpecialCabinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialCabinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialCabinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
