import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuffetSystemComponent } from './buffet-system.component';

describe('BuffetSystemComponent', () => {
  let component: BuffetSystemComponent;
  let fixture: ComponentFixture<BuffetSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuffetSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuffetSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
