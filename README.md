# **RESTAURENT MANAGEMENT SYSTEM**
      Based on your Technical and Functional Requirements,I have done coding and desiging part on Restaurent Management System. 
      I would like to describe step by step procedure of assignment
## Installation Process:--

      For developing this assignment i have used 
      TYPESCRIPT programming language and ANGULAR webframework.
- step by step procedure of installation:-
- installation of Angular:-
         for installing Angular  we need to environment by   installing node and npm,
         by default npm comes with node.
        for installing I visited the website 
[podmedics](https://nodejs.org/en"to the site")
    to check the version of node I used **node -v**
    and version of my node.org is **v10.15.0** and npm version is **6.4.1**
    next command for Angular
    **npm install -g @angular/cli** and
    version is **v10.15.0**
    next i have created new app **(ng Restarent-Management-System)**
    I used  editor code.visualstudio.com 
    to start this run a **(code .)** command
## CODING PART:-
 first I created components:
    **syntax:-ng g c component-name**
- customer
- cleaner
- waiter
- menu
- cookingDepartment
- foodServer
- feedback
- billPayment
- parkingPlaces
- homeDelivary
- tableBookingSystem(optinal)
- manager
## app.component.ts:-
 import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { template, text } from '@angular/core/src/render3';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import {Restarent} from './restarent/restaren'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl'],
})
export class AppComponent   {
 Title="Restaurant Management System";
}

## app.module.ts:-
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Route,Router} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { from } from 'rxjs';
import { CourseComponent } from './course/course.component';
import { CleanerComponent } from './cleaner/cleaner.component';
import { WaiterComponent } from './waiter/waiter.component';
import { CookingdepartmentComponent } from './cookingdepartment/cookingdepartment.component';
import { ServingComponent } from './serving/serving.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { BillpaymentComponent } from './billpayment/billpayment.component';
import { TablebookingComponent } from './tablebooking/tablebooking.component';
import { ParkingComponent } from './parking/parking.component';
import { RestroomsComponent } from './restrooms/restrooms.component';
import { ManagerComponent } from './manager/manager.component';
import { MenuComponent } from './menu/menu.component';
import { HomedeliveryComponent } from './homedelivery/homedelivery.component';
import { PARAMETERS } from '@angular/core/src/util/decorators';


@NgModule({
  declarations: [
    AppComponent,
    CourseComponent,
    CleanerComponent,
    WaiterComponent,
    CookingdepartmentComponent,
    ServingComponent,
    FeedbackComponent,
    BillpaymentComponent,
    TablebookingComponent,
    ParkingComponent,
    RestroomsComponent,
    ManagerComponent,
    MenuComponent,
    HomedeliveryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {path:'course',component:CourseComponent},
      {path:'cleaner',component:CleanerComponent},
      {path:'waiter',component:WaiterComponent},
      {path:'menu',component:MenuComponent},
    
      {path:'cookingdepartment',component:CookingdepartmentComponent},
      {path:'serving',component:ServingComponent},
      {path:'feedback',component:FeedbackComponent},
      {path:'billpayment',component: BillpaymentComponent},
      {path:'tablebooking',component:TablebookingComponent},
      {path:'parking',component:ParkingComponent},
      {path:'restrooms',component:RestroomsComponent},
      {path:'homedelivery',component:HomedeliveryComponent},
      {path:'manager',component:ManagerComponent},
      
    ])
],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

## app.component.html:-
 <div style = " text-align: center;background:pink; ">
<h1 style="color:red;">{{Title}}</h1>

 <img src="https://tse3.mm.bing.net/th?id=OIP.CpG1gbMmePQYgW7zqSGTTwHaEq&pid=15.1&P=0&w=287&h=182" width="436" height="275" />


  <h2 style="color:green;"><u> :The Restaurant System Like: </u></h2>
  </div>


<br>
<nav>
<a routerLink="course">CUSTOMER</a> 
</nav>
<nav>
<a routerLink="cleaner">CLEANER</a>
</nav>
<nav>
<a routerLink="waiter">WAITER</a>
</nav>
<nav>
  <a routerLink="menu">MENU</a>
  </nav>
<nav>

<a routerLink="cookingdepartment">COOKING-DEPARTMENT</a>
</nav>
<nav>
<router-outlet></router-outlet>
</nav>
<nav>
<a routerLink="serving">SERVING</a>
</nav>

<nav>

<a routerLink="feedback">FEEDBACK</a>

<router-outlet></router-outlet>
</nav>
<nav>
<a routerLink="billpayment">BILL-PAYMENT</a>
</nav>
<nav>
<a routerLink="tablebooking">TABLE-BOOKING</a>
</nav>
<nav>
<a routerLink="parking">PARKING</a>
</nav>
<nav>

<a routerLink="restrooms">REST-ROOMS</a>
</nav>
<router-outlet></router-outlet>
<nav>

<a routerLink="homedelivery">HOME-DELIVERY</a>
<router-outlet></router-outlet>
</nav>
<nav>
<a routerLink="manager">MANAGER</a>

<router-outlet></router-outlet>
</nav>

- For the usage of object i created file one file (restaren.ts)
export class Restarent {
    customer:string;
    cleaner:string;
    waiter:string;
    cookingDepartment:string;
    foodServer:string;
    feedback:string;
    billPayment:string;
    Restrooms:string;
    parkingPlaces:string;
    manager:string;
    homeDelivery:string;
    tableBooking:string;
    soup:string;
    chicken:string;
    vegsweetcorn:string;
    veghotsour:string;
    creamoftomato:string;
    chillichicken:string;
    chickenmanchurian:string;
    pepperchicken:string;
    apollofish:string;
    chillyfish:string;
    looseprawns:string;
    fishtikka:string;
    muttoncurry:string;
    muttonmasala:string;
    kheemamasala:string;

### The below code is for component.ts -ng g c component-name
- course.component.ts:-
- cleaner.component.ts:-
- waiter.component.ts
- menu.component.ts
- cookingdepartment.component.ts
- foodserving.component.ts
- feedback.component.ts
- billpayment.component.ts
- homedelivary.component.ts
- parkingplaces.component.ts
- tablebooking.component.ts
- manager..component.ts


 import { Component, OnInit } from '@angular/core';
import { Restarent } from '../restarent/restaren';
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.styl']
})
export class CourseComponent  {
restarent:Restarent = {
  customer:"Coustomer Coming ",
  cleaner:"Clean Boy Cleans And Supply  Water To Customers",
  waiter:"Waiter Takes Order From The Customer",
  cookingDepartment:"Cooking Department",
  foodServer:"Supply Boy Serves Food To The Customers",
  feedback:"Taking feedback from customers",
  billPayment:"Bill Paying",
  Restrooms:" Restrooms provided for customers",
  parkingPlaces:"Parking Places are also provided to the customers",
  manager:"Manager Manages All Above Things",
  homeDelivery:"Some Restarents provides Home Delivery",
  tableBooking:"Table Booking By phone call or in any other way",
  soup:"SWEET CORN SOUP",
  chicken:"CHICKEN HOT & SOUR",
  vegsweetcorn:"VEG SWEET CORN",
  veghotsour:"VEG HOT & SOUR",
  creamoftomato:"CREAM OF TOMATO",
  chickenmanchurian:"CHIKEN MANCHURIAN",
 chillichicken:"CHILLI CHICKEN",
 pepperchicken:"PEPPER CHIKEN", 
 apollofish:"APOLLO FISH",
 chillyfish:"CHILLY FISH",
 looseprawns:"LOOSE PRAWNS",
 fishtikka:"FISHTIKKA",
muttoncurry:"MUTTON CURRY",
muttonmasala:"MUTTON MASALA",
kheemamasala:"KHEEMA MASALA"
}
}

### html code for all components:-
course.component.html:
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSnB7rKKbbGvt8sw0v_vO-NQkKAi3Gvj4pvzaAXccRvhoH7MI9s" width="300" height="300"/>
  <p style="color:indigo;"><b><u> {{restarent.customer}} </u></b></p>

### cleaner.component.html:
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6p571I6YXRFHJ7efUrMZipzucjkya-p0hePkdtg2qj15gdcW0" width="338" height="280"/>
  <p  style="color:indigo;"><b><u>{{restarent.cleaner}} </u></b></p>

### waiter.component.html:
 <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi01djkL_kI2DQHcTzZkO7Z7pdG_YPCebobgMx4-xthwqA4aFO" width="350" height="241"/>
  <p style="color:indigo;" ><b><u> {{restarent.waiter}}</u> </b></p>

### menu.component.html:
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTL2yOWtCXZDnf4uPHBy7XiySeGPSrcFfSf3nKhqSSeqiHLW1Du" width="375" height="300"/>
  
  <p style="color:blue;" ><b><u>:::MENU::::</u></b></p>
  <p><i>   {{restarent.soup}} >  </i>   80/-</p>
  <p><i>{{restarent.chicken}}   80/-</i></p>
  <p style="color:brown;" ><b>VEG</b></p>
  <p><i>{{restarent.vegsweetcorn}}    80/-</i></p>
  <p><i>{{restarent.veghotsour}}      80/-</i></p>
  <p style="color:brown;"><b>CHINESE STARTERS - NON VEG </b></p>
  <p><i>{{restarent.creamoftomato}}   80/-</i></p>
  <p><i> {{restarent.chickenmanchurian}}  170/-</i></p>
  <p><i> {{restarent.chillichicken}}      170/-</i></p>
  <p><i> {{restarent.pepperchicken}}   170/-</i></p>
  <p style="color:brown;"><b>SEA FOOD STARTERS</b></p>
  <p><i>{{restarent.apollofish}}   240/-</i></p>
  <p><i> {{restarent.chillyfish}}  240/-</i></p>
  <p><i> {{restarent.looseprawns}}      240/-</i></p>
  <p><i> {{restarent.fishtikka}}   240/-</i></p>
  <p style="color:brown;"><b>MUTTON</b></p>
  <p><i> {{restarent.muttoncurry}}  210/-</i></p>
  <p><i> {{restarent.muttonmasala}}      210/-</i></p>
  <p><i> {{restarent.kheemamasala}}   210/-</i></p>
  
### cookingdepartment.component.html:
 <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ4CoT0vyY1nXybnFJQ7ZzmXg_FDFNe4IIufnuLqn2qS_TO5lNklA" width="400" height="250"/>
  <p style="color:indigo;"><b> <u>{{restarent.cookingDepartment}} </u></b></p>

### servering.component.html:
``` <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8CIdo4CHtqOVriazQJzbM3ZczlW-4rs16JPjOQA6UCl_DwlhMqw" width="380" height="214"/>
  <p style="color:indigo;"><b><u> {{restarent.foodServer}}</u> </b></p>
```
### feedback.component.html:
``` <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_lZTYlzzVrd6H2V5sCrd43wIvz9gOXscEPMs_7jhwHLnD614Ftw" width="275" height="256"/>
  <p style="color:indigo;"><b><u> {{restarent.feedback}} </u></b></p>
```
### billpayment.component.html:
``` <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR16H5y0ANMf6h5kp7Zlx9JqXSEKKYf3TLM63gj32yFFR44Un3SpQ" width="300" height="199"/>
  <p style="color:indigo;"><b> <u>{{restarent.billPayment}} </u></b></p>
```
### parkingplaces.component.html:
``` <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSumewnAOZv20UzQcF7J0927SY7zbbn9-NlIPiBxk-MQFo5TRJ7w" width="250" height="250"/>
  <p style="color:indigo;"><b><u> {{restarent.parkingPlaces}}</u></b> </p>
```
### tablebooking.component.html:
``` <img src ="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRgTp50GRUlZSmOz2dzcFNjjZex-4eORLC1ZRFYMDS3vG5edNQ1" width="300" height="300"/>
<p style="color:indigo;"><b><u> {{restarent.tableBooking}}</u> </b></p>
```
### manager.component.html:
``` <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSERV_BCRa9PjM805dGlp-hrGXFtrwtjkjoatNr2EbsVZaAzzwv" width="340" height="221"/>
  <p style="color:indigo;"><b><u>{{restarent.manager}}</u> </b></p>
```
### Designing Part:-
- Restaurent Management System as title,
- one restaurent image,
- I have used the navigation and routing concept for customer,cleaner,waiter,menu,cookingDepartment,FoodServing,feedback,BillPayment,parking,homeDelivery,TableBooking,Manager.
- For example if we click on customer it displays about customer,
- Suppose if we click on menu it displays the food items.


## Compilation And Running:-
             To compile the program we use command as (ng serve)
    next open a browser and type [podmedics](https://localhost:4200"to the site") 
    than we will get the  Restaurent Management System Web Design.
  




 





  










